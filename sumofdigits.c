#include<stdio.h>

int add(int x)
{
     int d,sum=0;
     int temp=x;
     do
     {
         d=temp%10;
         sum=sum+d;
         temp=temp/10;
     }while(temp!=0);
     return sum;
}

int main()
{
     int a;int s;
     printf("ENTER A NUMBER\n");
     scanf("%d",&a);
     s=add(a);
     printf("SUM OF THE DIGITS OF THE NUMBER %d IS %d\n",a,s);
     return 0;
}