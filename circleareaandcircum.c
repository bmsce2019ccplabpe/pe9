#include<stdio.h>
#include<math.h>
#define pi 3.14

float input()
{
     float r;
     printf("ENTER RADIUS OF CIRCLE\n");
     scanf("%f",&r);
    
}

float area(float r)
{
     float area1;
     area1=pi*r*r;
     return area1;
}
 
float circumference(float r)
{
     float circumference1;
     circumference1=2*pi*r;
     return circumference1;
}

void output1(float area1)
{
     printf("AREA OF CIRCLE IS %f\n",area1);
}

void output2(float circumference1)
{
     printf("CIRCUMFERENCE OF CIRCLE IS %f\n",circumference1);
}
     
     
int main()
{ 
     float a,c,radius;
     radius=input();
     a=area(radius);
     c=circumference(radius);
     output1(a);
     output2(c);
     return 0;
}
     